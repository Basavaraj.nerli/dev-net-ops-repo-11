pipeline {
    agent any
    stages {
        stage('Clean and prep workspace') {
            steps {
                git 'https://gitlab.com/Basavaraj.nerli/dev-net-ops-repo.git'
            }
        }
        stage('Staging') {
            steps {
                sh 'ansible-playbook playbook.yml'
            }
        }
    }
}
